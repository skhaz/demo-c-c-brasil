#ifndef MINHACLASSE_H
#define MINHACLASSE_H

#include <QObject>

#include <QtDebug>
#include <QStringList>

class MinhaClasse : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Rodrigo Delduca")
    Q_CLASSINFO("Site", "http://nullonerror.org")
    Q_PROPERTY(QStringList words READ words WRITE setWords)

    public:
        enum Numbers {
            One = 1,
            Two,
            Three
        };

        Q_ENUMS(Numbers);

        Q_INVOKABLE explicit MinhaClasse(QObject *parent = 0);

        Q_INVOKABLE void sayHello(const QString& value);

        void metodoNaoInvocavel();

        QStringList words();

        void setWords(const QStringList& words);

    signals:
        void sinal01(QString value);

    public slots:
        void slot01(QString value);
        void slot02();

    private:
        QStringList _words;
};

#endif
