#include "MinhaClasse.h"

#include <QtDebug>

#include <QMetaEnum>
#include <QMetaClassInfo>
#include <QMetaProperty>
#include <QMetaObject>
#include <QMetaMethod>

#include <boost/range/irange.hpp>

using boost::irange;

void demo_enums(QObject *object)
{
    // Get the meta object
    const QMetaObject *meta = object->metaObject();

    // Find the index of the desired enum
    int index = meta->indexOfEnumerator("Numbers");

    // Get the QMetaEnum instance
    QMetaEnum metaEnum = meta->enumerator(index);

    // Convert enum to string
    QString result = metaEnum.valueToKey(MinhaClasse::Two);
    qDebug() << result;

    // Convert string to enum
    bool ok = false;
    int value = metaEnum.keyToValue("One", &ok);
    if (ok)
        qDebug() << value;
}

void demo_classinfo(QObject *object)
{
    // Get the meta object
    const QMetaObject *meta = object->metaObject();

    // Iterate over all classes info, skipping the offset
    for (auto i : irange(meta->classInfoOffset(), meta->classInfoCount()))
    {
        QMetaClassInfo info = meta->classInfo(i);
        QString name = info.name();
        QString value = info.value();

        qDebug() << name << value;
    }
}

void demo_methods(QObject *object)
{
    // Get the meta object
    const QMetaObject *meta = object->metaObject();

    // Iterate over all methods, skipping the offset
    for (auto i : irange(meta->methodOffset(), meta->methodCount()))
    {
        QMetaMethod method = meta->method(i);

        qDebug() << "name" << method.name();
        qDebug() << "methodSignature" << method.methodSignature();
        qDebug() << "parameterNames" << method.parameterNames();
        qDebug() << "parameterTypes" << method.parameterTypes();
    }
}

void demo_properties(QObject *object)
{
    // Get the meta object
    const QMetaObject *meta = object->metaObject();

    // Iterate over all properties, skipping the offset
    for (auto i : irange(meta->propertyOffset(), meta->propertyCount()))
    {
        QMetaProperty property = meta->property(i);

        qDebug() << "name" << property.name();
        qDebug() << "typeName" << property.typeName();
        qDebug() << "isConstant" << property.isConstant();
        qDebug() << "isReadable" << property.isReadable();
        qDebug() << "isWritable" << property.isWritable();
        // qDebug() << "is ... " << property.is ...
    }
}

void demo_invokemethod(QObject *object)
{
    QMetaObject::invokeMethod(
        object,
        "sayHello",
        Q_ARG(QString, "world"));
}

int main()
{
    MinhaClasse minhaClasse;

    demo_enums(&minhaClasse);

    demo_classinfo(&minhaClasse);

    demo_methods(&minhaClasse);

    demo_properties(&minhaClasse);

    demo_invokemethod(&minhaClasse);

    return 0;
}
