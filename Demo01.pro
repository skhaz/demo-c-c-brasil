TEMPLATE = app

QT += core
QT -= gui

INCLUDEPATH += /usr/local/include

TARGET = Demo01
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle

OBJECTS_DIR = temp/obj
MOC_DIR = temp/moc
RCC_DIR = temp/rcc

SOURCES += main.cpp \
    MinhaClasse.cpp

HEADERS += \
    MinhaClasse.h

DISTFILES += \
    temp/moc/moc_MinhaClasse.cpp

