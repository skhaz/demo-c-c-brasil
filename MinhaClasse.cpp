#include "MinhaClasse.h"

MinhaClasse::MinhaClasse(QObject *parent)
: QObject(parent)
{
}

void MinhaClasse::sayHello(const QString& who)
{
    qDebug() << QString("Hello %1!").arg(who);
}

void MinhaClasse::metodoNaoInvocavel()
{
}

QStringList MinhaClasse::words()
{
    return _words;
}

void MinhaClasse::setWords(const QStringList& words)
{
    _words = words;
}

void MinhaClasse::slot01(QString value)
{
    Q_UNUSED(value)
}

void MinhaClasse::slot02()
{
}
